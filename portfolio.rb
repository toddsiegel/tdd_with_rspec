class Portfolio
 
  def initialize(holdings: {})
    @holdings = holdings
  end

  def holdings_count
    @holdings.count
  end

  def value
    value = 0
    @holdings.each do |stock, quantity|
      value += (stock.price * quantity) 
    end
    value
  end
end
