require_relative '../portfolio'

RSpec.describe Portfolio do

  context "a new portfolio" do
    it "has zero holdings" do
      portfolio = Portfolio.new

      expect(portfolio.holdings_count()).to eq(0)
    end
  end

  context "an existing portfolio" do
    it "should return the number of holdings it has" do
      holdings = { Object.new => 10 }
      portfolio = Portfolio.new(holdings: holdings)

      expect(portfolio.holdings_count).to eq(1)
    end
 
    it "should return its total value" do
      stock = instance_double("Stock", price: 30)

      holdings = { stock => 10 }
      portfolio = Portfolio.new(holdings: holdings)

      expect(portfolio.value).to eq(300)
    end
  end
end

